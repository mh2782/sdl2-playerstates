#include "Animation.h"

#include "SDL2Common.h"

//for printf()
#include <stdio.h>
#include <stdexcept>

Animation::Animation() {
    //Should be set by init - use frames to check for init not being called
    maxFrames = 0;
    frames = nullptr;

    //Need to be able to set this
    frameTimeMax = 0.7f;

    //Set current animation fram to first frame
    currentFrame = 0;

    //Zero frame time accumulator
    accumulator = 0;
}

void Animation::init(int noOfFrames,
    const int SPRITE_WIDTH, const int SPRITE_HEIGHT,
    int row, int col)
{
    // set frame count.
    maxFrames = noOfFrames;
    // allocate frame array
    frames = new SDL_Rect[maxFrames];
    //Setup animation frames - fixed row!
    for (int i = 0; i < maxFrames; i++)
    {
        if (row == -1)
        {
            frames[i].x = (i * SPRITE_WIDTH); //ith col.
            frames[i].y = (col * SPRITE_HEIGHT); //col row.
        }
        else
        {
            if (col == -1)
            {
                frames[i].x = (row * SPRITE_WIDTH); //ith col.
                frames[i].y = (i * SPRITE_HEIGHT); //col row.
            }
            else
            {
                // Throwing errors is more C++ than return values.
                throw std::runtime_error("Bad parameters to init");
            }
        }
        frames[i].w = SPRITE_WIDTH;
        frames[i].h = SPRITE_HEIGHT;
    }
}

void Animation::update(float timeDeltaInSeconds) {
    //Add elapsed time to the animation accumulator
    accumulator += timeDeltaInSeconds;

    //Check if animation needs update
    if (accumulator > frameTimeMax) {
        currentFrame++;
        accumulator = 0.0f;

        if (currentFrame >= maxFrames) {
            currentFrame = 0;
        }
    }
}

Animation::~Animation() {
    //Free the memory we allocated it with new so much use the matching delete operation
    delete[]frames;

    //Good practise to set pointers to null after deleting. This prevents accidental access
    frames = nullptr;
}

/**
* getCurrentFrame
*
* Get the current frame for rendering
* @return Pointer to the current frame of animation.
*
*/
SDL_Rect* Animation::getCurrentFrame()
{
    return &frames[currentFrame];
}
